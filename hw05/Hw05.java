//Dustin Hart CSE 002 10/10/18
//Calculate the probability that a certain hand will be dealt

import java.util.Scanner;
import java.util.Random;

public class Hw05 {

public static void main(String[] args) {
Scanner scan = new Scanner(System.in);
System.out.println("How many times should we deal out a hand: ");
int tries = scan.nextInt();
int times = tries;
int card1;
int card2;
int card3;
int card4;
int card5;
int count4 = 0;
int count3 = 0;
int count2 = 0;
int oneKind = 0;
//creating the while loop
Random rand = new Random ();
while (tries > 0) {

card1 = rand.nextInt(52) + 1;
card2 = rand.nextInt(52) + 1;
card3 = rand.nextInt(52) + 1;
card4 = rand.nextInt(52) + 1;
card5 = rand.nextInt(52) + 1;
// making the if statment so that we can ignore suits when matching for the pairs

card1 = card1%13;
card2 = card2%13;
card3 = card3%13;
card4 = card4%13;
card5 = card5%13;

//creating the if statment for if we get dealt a 4 of a kind
if ((card1 == card2 && card2 == card3 && card3 == card4 && card4 != card5) ||
(card1 == card2 && card2 == card3 && card3 == card5 && card4 != card5) ||
(card1 == card2 && card2 == card4 && card4 == card5 && card4 != card3) ||
(card1 == card3 && card3 == card4 && card4 == card5 && card1 != card2) ||
(card1 != card2 && card2 == card3 && card3 == card4 && card4 == card5)) {
count4++;
}
//creating the if statment for if we get dealt 3 of a kind
 if (card1 == card2 && card2 == card3 && card2 != card4 && card2 != card5 && card5 != card4 ||
card1 == card2 && card2 == card4 && card2 != card3 && card2 != card5 && card5 != card3 ||
card1 == card2 && card2 == card5 && card2 != card4 && card2 != card3 && card3 != card4 ||
card1 == card3 && card3 == card4 && card2 != card4 && card2 != card5 && card5 != card4 ||
card1 == card3 && card3 == card5 && card2 != card4 && card2 != card5 && card5 != card4 ||
card1 == card4 && card4 == card5 && card2 != card4 && card3 != card5 && card3 != card2 ||
card2 == card3 && card3 == card4 && card2 != card1 && card2 != card5 && card5 != card1 ||
card2 == card3 && card3 == card5 && card2 != card4 && card2 != card1 && card4 != card1 ||
card4 == card2 && card2 == card5 && card2 != card3 && card2 != card1 && card1 != card3 ||
card3 == card4 && card4 == card5 && card2 != card4 && card1 != card5 && card1 != card2) {
count3++;
}
  //else if statement for if it is a two pairs
else if (card1 == card2 && card3 == card4 && card1 != card5 && card3 != card5 ||
card1 == card3 && card2 == card4 && card1 != card5 && card2 != card5 ||
card1 == card4 && card2 == card3 && card1 != card5 && card2 != card5 ||
card1 == card3 && card2 == card4 && card1 != card5 && card2 != card5 ||
card1 == card2 && card4 == card5 && card1 != card3 && card4 != card3 ||
card1 == card3 && card2 == card5 && card1 != card4 && card2 != card4 ||
card1 == card3 && card5 == card4 && card1 != card2 && card2 != card5 ||
card1 == card4 && card2 == card5 && card1 != card3 && card2 != card3 ||
card1 == card4 && card3 == card5 && card1 != card2 && card2 != card5 ||
card1 == card5 && card2 == card3 && card1 != card4 && card2 != card4 ||
card1 == card5 && card2 == card4 && card1 != card3 && card2 != card3 ||
card1 == card5 && card3 == card4 && card1 != card2 && card2 != card3 ||
card2 == card3 && card4 == card5 && card1 != card5 && card2 != card1 ||
card2 == card4 && card3 == card5 && card1 != card5 && card2 != card1 ||
card2 == card5 && card3 == card4 && card1 != card5 && card1 != card3) {
count2++;
}
  //else if statment for if it is a pair.
else if (card1 == card2 && card2 != card3 && card2 != card4 && card2 != card5 && card3 != card4 && card3 != card5 && card4 != card5 ||
card1 == card3 && card2 != card3 && card3 != card4 && card3 != card5 && card2 != card4 && card2 != card5 && card4 != card5 ||
card1 == card4 && card2 != card3 && card2 != card4 && card2 != card5 && card3 != card4 && card3 != card5 && card4 != card5 ||
card1 == card5 && card2 != card3 && card2 != card4 && card2 != card5 && card3 != card4 && card3 != card5 && card4 != card5 ||
card2 == card3 && card3 != card1 && card3 != card4 && card3 != card5 && card1 != card4 && card1 != card5 && card4 != card5 ||
card2 == card4 && card3 != card1 && card3 != card4 && card3 != card5 && card1 != card4 && card1 != card5 && card4 != card5 ||
card2 == card5 && card3 != card1 && card3 != card4 && card3 != card5 && card1 != card4 && card1 != card5 && card4 != card5 ||
card3 == card4 && card4 != card1 && card4 != card2 && card4 != card5 && card1 != card2 && card1 != card5 && card2 != card5 ||
card3 == card5 && card4 != card1 && card4 != card2 && card4 != card5 && card1 != card2 && card1 != card5 && card2 != card5 ||
card4 == card5 && card4 != card1 && card4 != card2 && card4 != card3 && card1 != card2 && card1 != card3 && card2 != card3) {
oneKind++;
}
tries--;
}
  //converting the counts into porbabilities
double probability4 = (double)count4/times;
double probability3 = (double)count3/times;
double probability2 = (double)count2/times;
double probability1 = (double)oneKind/times;
//making it so that all of the porbabilities have 3 decimal places
String.format("%.3g%n", probability4);
String.format("%.3g%n", probability3);
String.format("%.3g%n", probability2);
String.format("%.3g%n", probability1);
  //printing out the results of the homework
System.out.println("The number of loops: " + times);
System.out.println("The probability of Four-of-a-kind: " + probability4);
System.out.println("The probability of Three-of-a-kind: " + probability3);
System.out.println("The probability of Two-pair: " + probability2);
System.out.println("The probability of a Pair: " + probability1);
}
}