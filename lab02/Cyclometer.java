// Dustin Hart
// 9/7/18
// CSE 002 MWF 11:10 - 12:10
// Create a code that takes 2 trips on bycicle and have the code print out the number of mints and count for the two trips, along with the distance in miles for each trip and the combined distance as well.


public class Cyclometer {
  // main method required for every java program
  public static void main(String[] args) {


int secsTrip1=480; //Seconds it took to complete the first trip.
int secsTrip2=3220; //Seconds it took to complete the second trip.
int countsTrip1=1561; //The number of counts in the first trip.
int countsTrip2=9037; //The number of counts in the first trip. 

double wheelDiameter=27.0, // the diameter of the whell
  PI=3.14159, // The value of PI 
  feetPerMile=5280, // The amount of feet in a mile 
  inchesPerFoot=12, //The amount of inches in a foot
  secondsPerMinute=60; //The amount of seconds in a minute
double distanceTrip1, distanceTrip2, totalDistance; // Naming the variables that will give us the distance of each trip and the total distance 
// Printing the amount of minutes and counts each trip had 
System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
// Distance of trips in inches below converted to miles 
distanceTrip1 = (countsTrip1*wheelDiameter*PI)/(inchesPerFoot*feetPerMile);
distanceTrip2 = (countsTrip2*wheelDiameter*PI)/(inchesPerFoot*feetPerMile);

// Finding the Total distance traveled in both trips 
totalDistance = distanceTrip1 + distanceTrip2;
//Printing out the output data 
System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
    
  } // end of main method
} // end of class
