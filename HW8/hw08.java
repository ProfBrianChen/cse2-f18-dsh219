//Dustin Hart
//CSE 002 HW07
//Create a deck of cards then shuffle the deck of cards and then print out a hand of 5 cards
//Import the scanner
import java.util.Scanner;
//import the random generator
import java.util.Random;
//create the class
public class hw08 {
    static String[] cards;
    //Creating the deck of cards
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // suits club, heart, spade or diamond
        String[] suitNames = { "C", "H", "S", "D" };
        String[] rankNames = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
        cards = new String[52];
        String[] hand = new String[5];
        int numCards = 5;
        int again = 1;
        int index = 51;
        for (int i = 0; i < 52; i++) {
            cards[i] = rankNames[i % 13] + suitNames[i / 13];
            System.out.print(cards[i] + " ");
        }
        System.out.println();
        printArray(cards);
      
        cards = shuffle(cards);
        printArray(cards);
        System.out.println("Hand");
        while (again == 1) {
            hand = getHand(cards, index, numCards);
            printArray(hand);
            index = index - numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
    }
//print the array of cards
    public static void printArray(String [] c){
        for(int i = 0; i< c.length; i++){
            System.out.print(c[i] + " ");
        }
        System.out.println("");
    }
//shuffle the cards
    public static String[] shuffle(String[] c){
        Random rand = new Random();
        for(int i = 0; i<51; i++){
            int r = Math.abs(rand.nextInt()) % 52;
            String tmp = c[r];
            c[r] = c[i];
            c[i] = tmp;
        }
        return c;
    }
//produce the hand made of 5 random cards
    public static String[] getHand(String[] list, int index, int numCards){
        String[] hand = new String[numCards];
        for(int i = index; i > index - numCards; i--){
            hand[index-i] = list[i];
        }
        return hand;
    }
}