// Finding the amount that the party will tip along with how much each member of the party has to pay
import java.util.Scanner;
// Adding class and main method
public class Check{
  public static void main(String[] args) {
  
  //Constructing the scanner
  Scanner myScanner = new Scanner( System.in );
  //Prompting user for check cost
  System.out.print("Enter the original cost of the check in the form xx.xx: ");
  //Accepting user input
  double checkCost = myScanner.nextDouble();
  //Tip percentage the user wants to pay
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
  //Accepting the input
  double tipPercent = myScanner.nextDouble();
  tipPercent /= 100; //converting the percentage into a decimal
  //Promopting the user for the number of people who went to the dinner and then accepting the input
  System.out.print("Enter the number of people who went out to dinner: ");
  int numPeople = myScanner.nextInt();
  //Creating the output for the amount taht each member of the group has to pay
  double totalCost;
  double costPerPerson;
  int dollars, dimes, pennies; //whole dollar amount of cost, for storing digits to the right of the decimal point for cost$
  totalCost = checkCost * (1 + tipPercent);
  costPerPerson = totalCost / numPeople;
  //get the whole amount dropping decimal fraction
  dollars=(int)costPerPerson;
  //dimes amount 
  dimes =(int) (costPerPerson * 10) % 10;
  pennies=(int) (costPerPerson * 100) % 10;
  System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
  
  
  
    }
}