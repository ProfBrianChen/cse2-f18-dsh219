//Dustin Hart CSE 002 10/10/18
//Create 2 arrays the first is 100 random integers and the second array stores the value of the occurances
//import random
import java.util.Random;
//creating the class
public class lab08 {
public static void main(String[] args) {
//Creating the first array
Random rand = new Random();
//Creating the random numbers in the array between 0 and 100
int[] randomArray = new int[100];
//Write the for statment for the random variables in the array
for(int i=0; i<randomArray.length; i++)
//This is the random statment behind the for statment using i
randomArray[i] =(int)(Math.random() * 100 +1);
//Start the counting of the rnadom ints in the array
int[] countA = countInts(randomArray);
//Display what we got in the first method
displayIntCount(countA);
}
//Make a second method
public static int[] countInts(int[] intD)
{
//Start creating the second array of how often each number was generated
int[] countB = new int[100];
//Creating a double nested for loop so that we can see how mnay of each number was generated randomly and keep track of them
for(int i=1; i<=countB.length; i++)
for(int k=0;k<intD.length;k++)
if(intD[k] == i)
countB[i-1]++;
return countB;
}
//Create the last method
public static void displayIntCount(int[] countD)
{
//The last for loop so that we can print out how frequently each number showed up
for (int i = 0; i < countD.length; i++)
//Creating the print statment
System.out.println((i+1) +" - " +countD[i] +" Times");
}
}
  