//Dustin Hart
//CSE 002 HW07
//Create the methods to make an array with 10 random integers between 0 and 9
//Import the scanner
import java.util.Scanner;
//create the class
public class RemoveElements {
    public static void main(String[] arg) {
        Scanner scan = new Scanner(System.in);
      //declaring the variables
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            while(index < 0 || index > num.length-1){
                System.out.println("Enter a valid index");
                index = scan.nextInt();
            }
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            while(target < 0 || target > num.length-1){
                System.out.println("Enter a valid index");
                target = scan.nextInt();
            }
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); // return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    }
//creating the random input method
    public static int[] randomInput() {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * array.length);
        }
        return array;
    }
//creating the delete method
    public static int[] delete(int[] list, int pos) {
        int[] array = new int[list.length - 1];
        for (int i = 0; i < list.length; i++) {
            if (i < pos) {
                array[i] = list[i];
            } else if (i > pos) {
                array[i - 1] = list[i];
            }
        }
        return array;
    }
//create the remove method 
    public static int[] remove(int[] list, int target) {
        int counter = 0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] == target) {
                counter++;
                list[i] = -1;
            }
        }
        int[] array = new int[list.length - counter];
        int tmp = 0;
        for(int i = 0; i < list.length; i ++){
            if(list[i] != -1){
                array[tmp] = list[i];
                tmp++;
            }
        }
        return array;
    }

}
