//Dustin Hart
//CSE 002 HW07
//create an array of inputs where youc an search for ints in the array
//Import the scanner
import java.util.Scanner;
//create class
class CSE2Linear {
    static int array[];

    public static void main(String[] args) {
        array = new int[15];
        Scanner scan = new Scanner(System.in);
      //creating a for loop with while if statments in it
        for (int i = 0; i < 15; i++) {
            System.out.println("Please enter an int");
            int tmp;
            while (!scan.hasNextInt()) {
                scan.next();
                System.out.println("Please enter an int");
            }
            tmp = scan.nextInt();
            // array[i] = tmp;
            if (i >= 1) {
                if (array[i - 1] < tmp && tmp > 0 && tmp < 101) {
                    array[i] = tmp;
                } else {
                    i--;
                }
            } else {
                if (tmp > 0 && tmp < 101) {
                    array[i] = tmp;
                } else {
                    i--;
                }
            }

        }
      //second for loop with while if statments in it
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("Please enter a grade to search for: ");
        while (!scan.hasNextInt()) {
            scan.next();
            System.out.println("Please enter an int");
        }
        int z = scan.nextInt();
        if(binarySearch(z) == -1){
            System.out.println("Grade not found");
        } else {
            System.out.println("Grade found in " + binarySearch(z) + " iterations");
        }

        array = randomize(array);
        System.out.println("Scambled");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.out.println("Please enter a grade to search for: ");
        while (!scan.hasNextInt()) {
            scan.next();
            System.out.println("Please enter an int");
        }
        z = scan.nextInt();
        if(linearSearch(z) == -1){
            System.out.println("Grade not found");
        } else {
            System.out.println("Grade found in " + linearSearch(z) + " iterations");
        }

    }
//creating the method binary search
    public static int binarySearch(int key) {
        int count = 0;
        int start = 0;
        int end = array.length - 1;
        int tmp;
        while (start <= end) {
            count++;
            tmp = (end + start) / 2;
            if (array[tmp] < key) {
                start = tmp + 1;
            } else if (array[tmp] > key) {
                end = tmp - 1;
            } else {
                return count;
            }
        }
        return -1;
    }
//creating the method linearSearch
    public static int linearSearch(int key) {
        int count = 0;
        while (count < array.length) {
            if (array[count] == key) {
                count++;
                return count;
            }
            count++;
        }
        return -1;
    }
//creating the method randomize
    public static int[] randomize(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int r = (int) (Math.random() * array.length);
            int tmp = array[r];
            array[r] = array[i];
            array[i] = tmp;
        }
        return array;
    }
}