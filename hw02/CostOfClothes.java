


public class CostOfClothes{
  
  public static void main(String args[]){
    
    int numPants = 3; //Number of pairs of pants
    int numShirts = 2; //Number of sweatshirts
    int numBelts = 1; //Number of belts
    double pantsPrice = 34.98; //Cost per pair of pants 
    double shirtPrice = 24.99; //Cost per shirt 
    double beltCost = 33.99; //cost per belt 
    double paSalesTax = 0.06; //Tax Rate
    
    double pantsCost = numPants * pantsPrice; //Total cost of the pants bought
    System.out.println("Total cost of pants is " + pantsCost);
    double shirtsCost = numShirts * shirtPrice; //Total cost of sweatshirts
    System.out.println("Total cost of sweatshirts is " + shirtsCost);
    double beltsCost = numBelts * beltCost; //Total cost of belts 
    System.out.println("Total cost of belts is " + beltsCost);
    
    double pantsSalesTax = pantsCost * paSalesTax;
    System.out.println("The sales tax for pants is " + pantsSalesTax);
    double shirtsSalesTax = shirtsCost * paSalesTax;
    System.out.println("The sales tax for shirts is " + shirtsSalesTax);
    double beltsSalesTax = beltsCost * paSalesTax;
    System.out.println("The sales tax for belts is " + beltsSalesTax);
    
    double totalCostNoTax = pantsCost + shirtsCost + beltsCost;
    System.out.println("The total cost before tax is " + totalCostNoTax);
    
    double totalSalesTax = pantsSalesTax + shirtsSalesTax + beltsSalesTax;
    System.out.println("The total sales tax is " + totalSalesTax);
    
    double totalPaid = totalCostNoTax + totalSalesTax;
    System.out.println("The total paid for this transaction is " + totalPaid);
     
    
  }
}