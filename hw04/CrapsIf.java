//Dustin Hart CSE 002 9/25/18
//Naming the combination of die rolled with common craps slang

//importing the random generator into the file
import java.util.Random;
//importing the scanner into the file
import java.util.Scanner;
//creating the class and string for the file
public class CrapsIf{
  public static void main(String[] args){
    //creating the scanner in the file
    Scanner myScanner = new Scanner(System.in);
    //Creating the random number generator for the die
    Random rand = new Random();
    //create value for the first die
    int dice1 = rand.nextInt(6) + 1;
    //create value for the second die
    int dice2 = rand.nextInt(6) + 1;
    //Creating the sum function for the two die rolled
    int sumDice = dice1 + dice2;
    //Creating the strings for when we as the user to input yes or no
    String yes = "yes";
    String no = "no";
    //Creating the code to prompt the user to ask if they want to chose the numbers rolled or not
    System.out.println("Would you like to choose the numbers rolled, or would you like a randomly casted die?");
    System.out.println("Type yes if you want to choose your numbers, or type no if you want them randomly generated.");
    
    String decision = myScanner.nextLine();
    
    //Create an if statment for when they type yes
    if (decision.equals("yes")){
     
      System.out.println("Type in your first value (From 1-6)");
      //Assigning the value typed above as an integer
      int choice1 = myScanner.nextInt();
      System.out.println("Type in your second value (From 1-6)");
      //Assigning the second value as an integer
      int choice2 = myScanner.nextInt();
      //Summing up the inputted numbers by the user
      int diceSum = choice1 + choice2;
     //Creating the output if the numbers are the same
      if (choice1 == choice2) {
        if(choice1 == 1) {
          System.out.println("You have rolled Snake Eyes.");
        } else if (choice1 == 6) {
          System.out.println("You have rolled Boxcars");
        } else {
          System.out.println("You have rolled a Hard " + diceSum);
        }}
      //Creating the output for if the numbers are different
        else if (diceSum == 3) {
          System.out.println("You have rolled an Ace Duece");
        }
        else if (diceSum == 4) {
          System.out.println("You have rolled an Easy Four");
        }
        else if (diceSum == 5) {
          System.out.println("You have rolled a Fever Five");
        }
        else if (diceSum == 6) {
          System.out.println("You have rolled an Easy Six");
        }
        else if (diceSum == 7) {
          System.out.println("You have rolled a Seven out");
        }
        else if (diceSum == 8) {
          System.out.println("You have rolled an Easy Eight");
        }
        else if (diceSum == 9) {
          System.out.println("You have rolled a Nine");
        }
        else if (diceSum == 10) {
          System.out.println("You have rolled an Easy Ten");
        }
        else {
          System.out.println("You have rolled a Yo-leven");
        }
      }
    //This is the end of the code that is involved with the user writing in their own numbers
    // Below will be the code that assigns a value if they chose for random numers to be given to them
  else {
      //Giving the user their first random value
      System.out.println(dice1 + " is your first number.");
      //Giving the user their second random value
      System.out.println(dice2 + " is your second number.");
    //Writing the code for when the die are the same
      if (dice1 == dice2) {
        if(dice1 == 1) {
          System.out.println("You have rolled Snake Eyes.");
        } else if (dice1 == 6) {
          System.out.println("You have rolled Boxcars");
        } else {
          System.out.println("You have rolled a Hard " + sumDice);
        }}
      //Creating the output for if the numbers are different
        else if (sumDice == 3) {
          System.out.println("You have rolled an Ace Duece");
        }
        else if (sumDice == 4) {
          System.out.println("You have rolled an Easy Four");
        }
        else if (sumDice == 5) {
          System.out.println("You have rolled a Fever Five");
        }
        else if (sumDice == 6) {
          System.out.println("You have rolled an Easy Six");
        }
        else if (sumDice == 7) {
          System.out.println("You have rolled a Seven out");
        }
        else if (sumDice == 8) {
          System.out.println("You have rolled an Easy Eight");
        }
        else if (sumDice == 9) {
          System.out.println("You have rolled a Nine");
        }
        else if (sumDice == 10) {
          System.out.println("You have rolled an Easy Ten");
        }
        else{
          System.out.println("You have rolled a Yo-leven");
    
    }
    
    }
  
  }
}

