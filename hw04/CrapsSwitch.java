//Dustin Hart CSE 002 9/25/18
//Naming the combination of die rolled with common craps slang

//importing the random generator into the file
import java.util.Random;
//importing the scanner into the file
import java.util.Scanner;
//creating the class and string for the file
public class CrapsSwitch {
  public static void main(String[] args){
    //creating the scanner in the file
    Scanner myScanner = new Scanner(System.in);
    //Creating the random number generator for the die
    Random rand = new Random();
    //create value for the first die
    int dice1 = rand.nextInt(6) + 1;
    //create value for the second die
    int dice2 = rand.nextInt(6) + 1;
    //Creating the sum function for the two die rolled
    int sumDice = dice1 + dice2;
    //Creating the strings for when we as the user to input yes or no
    String yes = "yes";
    String no = "no";
    //Creating the code to prompt the user to ask if they want to chose the numbers rolled or not
    System.out.println("Would you like to choose the numbers rolled, or would you like a randomly casted die?");
    System.out.println("Type yes if you want to choose your numbers, or type no if you want them randomly generated.");
    
    String decision = myScanner.nextLine();
    //Starting the switch 
    switch (decision) {
      //Creating the first switch for if the user inputs yes
      case "yes": 
        System.out.println("Type in your first value (From 1-6)");
        int choice1 = myScanner.nextInt();
        System.out.println("Type in your second value (From 1-6)");
        int choice2 = myScanner.nextInt();
        //Summing up the inputted numbers by the user
         int diceSum = choice1 + choice2;
        boolean sameNumber;
        sameNumber = (choice1 == choice2);
        String doubles = String.valueOf(sameNumber);
        //System.out.println(doubles);
        switch (doubles){
          case "true":
          switch (diceSum) {
          //Producing the output for each die combination
           case 2: System.out.println("You have rolled Snake Eyes");
            break;
           case 12: System.out.println("You have rolled Boxcars");
              break;
            default: System.out.println("You have rolled a Hard " + diceSum);
              break;}
              break;
          case "false":
            switch (diceSum) {
              case 3: System.out.println("You have rolled an Ace Deuce");
                break;
              case 4: System.out.println("You have rolled an Easy Four");
                break;
              case 5: System.out.println("You have rolled a Fever Five");
                break;
              case 6: System.out.println("You have rolled an Easy Six");
                break;
              case 7: System.out.println("You have rolled a Seven Out");
                break;
              case 8: System.out.println("You have rolled an Easy Eight");
                break;
              case 9: System.out.println("You have rolled a Nine");
                break;
              case 10: System.out.println("You have rolled an Easy Ten");
                break;
              case 11: System.out.println("You have rolled an Yo-leven");
                break;
              default: System.out.println("Invalid");
                break;}
               break;
          default: break; 
        }
        break;
      case "no":
         //Giving the user their first random value
      System.out.println(dice1 + " is your first number.");
      //Giving the user their second random value
      System.out.println(dice2 + " is your second number.");
        boolean numberSame;
        numberSame = (dice1 == dice2);
        String duplicate = String.valueOf(numberSame);
        //Writing the code for when the die are the same
     switch (duplicate){
          case "true":
          switch (sumDice) {
          //producing the output for each die combination
           case 2: System.out.println("You have rolled Snake Eyes");
            break;
           case 12: System.out.println("You have rolled Boxcars");
              break;
            default: System.out.println("You have rolled a Hard " + sumDice);
              break;}
              break;
          case "false":
            switch (sumDice) {
              case 3: System.out.println("You have rolled an Ace Deuce");
                break;
              case 4: System.out.println("You have rolled an Easy Four");
                break;
              case 5: System.out.println("You have rolled a Fever Five");
                break;
              case 6: System.out.println("You have rolled an Easy Six");
                break;
              case 7: System.out.println("You have rolled a Seven Out");
                break;
              case 8: System.out.println("You have rolled an Easy Eight");
                break;
              case 9: System.out.println("You have rolled a Nine");
                break;
              case 10: System.out.println("You have rolled an Easy Ten");
                break;
              case 11: System.out.println("You have rolled an Yo-leven");
                break;
              default: System.out.println("Invalid");
                break;}
               break;
          default: break; 
        }
        break;  
      default: break;
    }
        
    
    
    }
  }
