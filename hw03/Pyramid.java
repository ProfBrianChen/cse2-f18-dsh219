//Dustin Hart
//CSE 002
//9/18/18
//Given the dimensions 
import java.util.Scanner;
//Adding class and main method
public class Pyramid{
  public static void main(String[] args) {
    //Constructing the scanner
    Scanner myScanner = new Scanner( System.in );
    //Prompt the user to input the length
    System.out.print("The square side of the pyramid is (input length): ");
    //Accepting the user input
    int length = myScanner.nextInt();
    //Convert length into square dimensions
    int square = length * length;
    //Prompt the user to input the length
    System.out.print("The height of the pyramid is (input height): ");
    //Accepting the user input
    int height = myScanner.nextInt();
    //Defining variable pyramidVolume
    int pyramidVolume;
    //Calculating volume of the pyramid
    pyramidVolume = (square * height)/3;
    //Printing the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + pyramidVolume + ".");
    
    
    
    
  }
}
    