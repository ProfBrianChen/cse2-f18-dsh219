//Dustin Hart
//CSE 002
//9/18/18
//Take the amount of land affected by a hurricane and find out the average rain dropped on it
import java.util.Scanner;
//Adding class and main method
public class Convert{
  public static void main(String[] args) {
    //Constructing the scanner
    Scanner myScanner = new Scanner( System.in );
    // Prompt the user for affected area in acres
    System.out.print("Enter the affected area in acres: ");
    //Accepting the user input
    double affectedArea=myScanner.nextDouble();
    //Convert acres into squared miles
    affectedArea *= 0.0015625;
    // Prompt the user for the rainfall in the affected area
    System.out.print("Enter the rainfall in the affected area: ");
    //Accepting the user input
    double rainfallAmount=myScanner.nextDouble();
    //Convert inches into miles
    rainfallAmount /= 63360;
    //Declaring the variable quantityRain
    double quantityRain;
    //Calculating quantity of rain
    quantityRain = affectedArea * rainfallAmount;
    //Printing the amount of rain in cubic miles
    System.out.print(quantityRain + " cubic miles");
    
  }
}