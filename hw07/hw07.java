//Dustin Hart
//CSE 002 HW07
//Practice writing methods, using strings and making the user use the correct input
//Import the scanner
import java.util.Scanner;
//Create the class
public class hw07 {
static Scanner scan;
static String sample;
public static void main(String[] args){
scan = new Scanner(System.in);
sample  = "";
sampleText();
//Initializing all of the options for the menu
char tmp = printMenu();
while(tmp != 'q'){
if(tmp == 'c'){
System.out.println(getNumOfNonWSCharacters(sample));
} else if(tmp == 'w'){
System.out.println(getNumOfWords(sample));
} else if(tmp == 'f'){
System.out.println("Enter a word or phrase to be found: ");
String t2 = scan.nextLine();
String sub = scan.nextLine();
System.out.println("\"" + sub + "\" instances: " + findText(sample, sub));
} else if(tmp == 'r'){
System.out.println(replaceExclamation(sample));
} else if(tmp == 's'){
System.out.println(shortenSpace(sample));
}
tmp = printMenu();
}
}
//Prompting the user to input the text that they want to use
static void sampleText(){
System.out.println("Enter a sample text:");
sample = scan.nextLine();
System.out.println("You entered: " + sample);
}
//Producing the mneu with all of the options to evaluate the inputted text
static char printMenu(){
System.out.println("MENU\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit");
return scan.next().charAt(0);

}
//Creating the process to find the non white space characters
static int getNumOfNonWSCharacters(String s){
int count = 0;
for(int i = 0; i<s.length(); i++){
if(s.charAt(i) != ' '){
count ++;
}
}
return count;
}
//Creating the process to find the number of words in the text
static int getNumOfWords(String s){
int count = 0;
for(int i = 0; i < s.length() -1 ; i++){
if(s.charAt(i) == ' '){
count++;
while(s.charAt(i) == ' '){
i++;
}
}
}
count++;
return count;
}
//Creating the find text process
static int findText(String s, String sub){
int count = 0;
for(int i = 0; i < s.length() - sub.length(); i++){
if(s.charAt(i) == sub.charAt(0)){
if(s.substring(i, i+sub.length()).equals(sub)){
count++;
}
}
}
return count;
}
//Creating the process to replace all ! with .
public static String replaceExclamation(String s){
String tmp = s;
for(int i = 0; i < s.length(); i++){
if(s.charAt(i) == '!'){
s = s.substring(0, i) + '.' + s.substring(i+1);
}
}
return s;
}
//Creating the process to make any place wher ethere are 2 spaces into 1
public static String shortenSpace(String s){
String tmp = s;
for(int i = s.length()-1; i > 0 ; i--){
if(s.charAt(i) == ' '){
if(s.charAt(i-1) == ' '){
int t = i + 1;
s = (s.substring(0 , i)) + s.substring(t);
i--;
}
}
} 
return s;
}
}
