//Dustin Hart CSE 002 10/10/18
//Create a pattern that outputs a bunch of stars and leaves spaces that create the symbol X
//import scanner
import java.util.Scanner;
//creating the class
public class EncryptedX {
//Create the method
public static void main(String[] args) {
Scanner scan = new Scanner(System.in);
System.out.println("How many rows do you want the X to be made up of: ");
int input = scan.nextInt();

for(int numRows = 0; numRows <= input; numRows++){
//Controls what is outputted into each row
for(int length = 0; length <= input; length++){
if (numRows == length || numRows == input-length){
System.out.print(" ");}
else { System.out.print("*");}
}
System.out.println();
}
}
}