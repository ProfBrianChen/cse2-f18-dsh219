//Dustin hart
//CSE 002
//creating a bunch of different methods and with those methods having them randomly generate sentences 
import java.util.Random;
//creating the public class
public class method {
static Random randomGenerator;
//The main method
public static void main(String[] args){
randomGenerator = new Random();
finalMethod();

}

//The final method
public static void finalMethod(){
String subject = subject();
String object = object();
int r = randomGenerator.nextInt(9);
System.out.println("The " + subject + " " + verb() + " the " + adjective() + " " + object + ".");
for(int i = 0; i < r; i++){
actionSentance(subject);
}
System.out.println("The " + subject + " " + conclVerb() + " the " + object + "!");
}
//Creating the adjective method
public static String adjective(){
int r = randomGenerator.nextInt(9);
String str = "";
switch(r) {
case 1: str= "big";
break;
case 2: str= "happy";
break;
case 3: str= "quick";
break;
case 4: str= "lazy";
break;
case 5: str= "slow";
break;
case 6: str= "fast";
break;
case 7: str= "small";
break;
case 8: str= "heavy";
break;
case 9: str= "sad";
break;
default: str = "silly";
break;
}
return str;
}
//Creating the subject method
public static String subject(){
int r = randomGenerator.nextInt(9);
String str = "";
switch(r) {
case 1: str= "mouse";
break;
case 2: str= "dog";
break;
 case 3: str= "cat";
break;
case 4: str= " goat";
break;
case 5: str= "goose";
break;
case 6: str= "duck";
break;
case 7: str= "elephant";
break;
case 8: str= "mouse";
break;
case 9: str= "lamb";
break;
default: str = "rock";
break;
}
return str;
}
//Creating the verb method
public static String verb(){
int r = randomGenerator.nextInt(9);
String str = "";
switch(r) {
case 1: str= "jumped";
break;
case 2: str= "tickled";
break;
case 3: str= "saw";
break;
case 4: str= "ate";
break;
case 5: str= "passed";
break;
case 6: str= "run";
break;
case 7: str= "hop";
break;
case 8: str= "speak";
break;
case 9: str= "bounce";
break;
default: str = "chased";
break;
}
return str;
}
//Creating the object method
public static String object(){
int r = randomGenerator.nextInt(9);
String str = "";
switch(r) {
case 1: str= "fox";
break;
case 2: str= "moose";
break;
case 3: str= "cow";
break;
case 4: str= "janitor";
break;
case 5: str= "wolf";
break;
case 6: str= "dog";
break;
case 7: str= "him";
break;
case 8: str= "her";
break;
case 9: str= "baby";
break;
default: str = "ghost";
break;
}
return str;
}
//Creating the conclverb method
public static String conclVerb(){
int r = randomGenerator.nextInt(9);
String str = "";
switch(r) {
case 1: str= "loved";
break;
case 2: str= "befriended";
break;
default: str = "liked";
break;
}
return str;
}
//Creating the actionSentance
public static void actionSentance(String subject){
int r = randomGenerator.nextInt(1);
if(r == 0){
System.out.println("It " + verb() + " the " + adjective() + " " + object() + ".");
} else {
System.out.println("The " + subject() +  " " +verb() +" the " + adjective() + " " + object()+ ".");
}

}

}
