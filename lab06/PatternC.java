//Dustin Hart CSE 002 10/10/18
//Create a pattern that outputs an amount of integers into a triangular shape
//import scanner
import java.util.Scanner;
//creating the class
public class PatternC {

public static void main(String[] args) {
Scanner scan = new Scanner(System.in);
System.out.println("How many integers do you want the length if the pyramid to be: ");
//defining the scanned in int
int input = scan.nextInt();
//create the pattern of a pyramid
//Controls the number of rows
for(int numRows = 1; numRows <= input; numRows++){
//Create the spaces so it is lined up on the right margin
for(int space = (input-1); space >= numRows; space--){
System.out.print("  ");
}
//Controls what is outputted into each row
for(int length = numRows; length >= 1; length--){
System.out.print(length + " ");
}
System.out.println(" ");
}
}
}