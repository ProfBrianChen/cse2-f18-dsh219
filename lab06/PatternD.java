//Dustin Hart CSE 002 10/10/18
//Create a pattern that outputs an amount of integers into a triangular shape
//import scanner
import java.util.Scanner;
//creating the class
public class PatternD {

public static void main(String[] args) {
Scanner scan = new Scanner(System.in);
System.out.println("How many integers do you want the length if the pyramid to be: ");
//defining the scanned in int
int input = scan.nextInt();
//create the pattern of a pyramid
for (int rows = input; rows >= 1; rows--){
for (int number = rows; number >= 1; number--){
System.out.print(number + " ");
}
System.out.println();
}
}
}