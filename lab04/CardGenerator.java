//Dustin Hart
//CSE 002
//9/22/18
//Randomly generator a card out of the 52 cards in a deck. Print the number and suit
//Import java.util.random into the program
import java.util.Random;
//create the class
public class CardGenerator {
  // create the string
  public static void main(String[] args){
    
    //Assigning the random number generator
    Random random = new Random();
      //Creating the numbers being generated to be from 1-52
    int Card = random.nextInt(52) + 1;
    
    //Create the if statments to produce the random card and its suit
    //Creating the operations for Diamonds
    if (Card <= 13) {
      Card = Card;
      //Assigning Face cards and aces to certain numbers
      if (Card == 1){
        System.out.println("Ace of Diamonds");}
      else if (Card == 11){
        System.out.println("Jack of Diamonds");}
      else if (Card == 12){
        System.out.println("Queen of Diamonds");}
      else if (Card == 13){
        System.out.println("King of Diamonds");}
      else System.out.println(Card + " of Diamonds");}
     //Creating the operations for Clubs      
     if (13 < Card && Card <= 26) {
      Card = Card - 13;
      if (Card == 1){
        //Assigning Face cards and aces to certain numbers
        System.out.println("Ace of Clubs");}
      else if (Card == 11){
        System.out.println("Jack of Clubs");}
      else if (Card == 12){
        System.out.println("Queen of Clubs");}
      else if (Card == 13){
        System.out.println("King of Clubs");}
      else System.out.println(Card + " of Clubs");}   
    //Creating the operations for hearts
    if (26 < Card && Card <= 39) {
      Card = Card - 26;
      if (Card == 1){
        //Assigning Face cards and aces to certain numbers
        System.out.println("Ace of Hearts");}
      else if (Card == 11){
        System.out.println("Jack of Hearts");}
      else if (Card == 12){
        System.out.println("Queen of Hearts");}
      else if (Card == 13){
        System.out.println("King of Hearts");}
      else System.out.println(Card + " of Hearts");} 
    //Creating the operations for spades
    if (39 < Card && Card <= 52) {
      Card = Card - 39;
      if (Card == 1){
        //Assigning Face cards and aces to certain numbers
        System.out.println("Ace of Spades");}
      else if (Card == 11){
        System.out.println("Jack of Spades");}
      else if (Card == 12){
        System.out.println("Queen of Spades");}
      else if (Card == 13){
        System.out.println("King of Spades");}
      else System.out.println(Card + " of Spades");} 
  }
}